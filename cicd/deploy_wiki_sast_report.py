""" Copyright (C) 2024  Matti Kaupenjohann

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

import os
from pathlib import Path

import gitlab

from python_utils.load_data import WikiDataloader


def main():
    gitlab_url = os.environ.get('GITLAB_URL') or "https://gitlab.com"
    gl = gitlab.Gitlab(gitlab_url, private_token=os.environ.get('SAST_REPORT'))
    project_id = os.environ.get('CI_PROJECT_ID')
    project = gl.projects.get(project_id)
    pages = project.wikis.list()
    sast_report_path = Path(os.environ.get('CI_PROJECT_DIR')) / "gl-sast-report.json"
    content_loader = WikiDataloader(sast_report_path)
    page_titles = dict()
    if pages:
        for idx, page in enumerate(pages):
            page_titles[page.title] = idx

    if "SAST REPORT" in page_titles.keys():
        idx = page_titles["SAST REPORT"]
        pages[idx].content = content_loader.generate_content()
        pages[idx].save()
    else:
        title = "SAST-REPORT"
        content = content_loader.generate_content()
        page = project.wikis.create(
            {
                "title": title,
                "content": content
            }
        )


if __name__ == "__main__":
    main()

