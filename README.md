# SAST-WIKI-DASHBOARD

![How it could look like](SAST.png)

**THIS README CAN BE REPLACED AFTER SUCCESSFUL IMPLEMENTATION**

## Getting started

This project can be used to set up an arbitary project with a working sast-pipeline, which reports to a wiki page. The SAST-report gives insides to vulnerbilities created by the developer. 

There are a few steps necessary to enable the complete feature, otherwise the pipeline will fail:

1. Create the access token.
1. Save the access token as CI/CD secret.
1. Activate CI/CD runner(s).
1. Test the Pipeline.

### 1. Create access token

Menuentry: $`\textcolor{orange}{\text{"Settings > Access Tokens"}}`$ 

- Token name: `SAST-REPORT`
- Expiration date: `delete`
- Select a role: `Maintainer`
- Select Scopes: `api`

**Leave the page open or copy the token to clipboard** 

### 2. Save CI/CD Secret

Menuentry: $`\textcolor{orange}{\text{"Settings > CI/CD"}}`$ 

- Expand "Variables":
    - "Add variable":
        - Key: `SAST_REPORT`
        - Value: `<copied-access-token>`
        - Flags: all selected

Step 1. and 2. could also be accomplished on group level by a group maintainer.

> Add additional Variable `GITLAB_URL` if you are running on self hosted instance

### 3. Activate Runner

Menuentry: $`\textcolor{orange}{\text{"Settings > CI/CD"}}`$ 

Check if runners are available.

### 4. Test Pipeline

Menuentry: $`\textcolor{green}{\text{"CI/CD > Pipelines"}}`$ 

- Button "Run pipeline" at upper right
    - Button "Run pipeline"

## SAST-REPORT

The SAST-REPORT can be accessed via the [wiki/SAST-REPORT](../../wikis/SAST-REPORT)
